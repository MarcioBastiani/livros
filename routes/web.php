<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Livros
$router->group(['prefix' => 'livros'], function () use ($router) {
    $router->get('/', 'LivroController@list');
    $router->get('/{id}', 'LivroController@show');
    $router->post('/', 'LivroController@store');
    $router->put('/{id}', 'LivroController@update');
    $router->delete('/{id}', 'LivroController@delete');
});