<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Exception;
use App\Models\Livro;

class LivroController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function list(Request $request) {
        //Filtros
        $filtro_nome = $request->input('nome');
        
        if ($filtro_nome) {
            $livros = Livro::where('nome','like', '%'.$filtro_nome.'%')->orderBy('nome')->get();
        }
        else {
            $livros = Livro::orderBy('nome')->get();
        }

        if (count($livros)==0) {
            $retorno = array(
                "erro" => "Nenhum registro encontrado"
            );
    
            return response()->json($retorno, 200);
        }

        return response()->json($livros, 200);
    }

    public function show($id) {
        $livro = Livro::find($id);
        
        if ($livro) {
            return response()->json($livro);
        }
        else {
            $retorno = array(
                "mensagem" => 'ID não encontrado'
            );
            
            return response()->json($retorno, 404);
        }
    }

    public function store(Request $request) {
        $dados = $request->all();
        
        try {
            $livro = Livro::create($dados);

            return response()->json($livro, 201);
        }
        catch (QueryException $ex) {
            $retorno = array(
                "erro" => 'Erro ao criar o registro',
                "detalhes" => $ex->getMessage()
            );
            return response()->json($retorno, 500);
        }
        catch (Exception $ex) {
            $retorno = array(
                "erro" => 'Erro ao criar o registro',
                "detalhes" => $ex->getMessage()
            );
            return response()->json($retorno, 500);
        }
    }

    public function update($id, Request $request) {
        $dados = $request->all();

        //Procura e posiciona o registro
        $livro = Livro::find($id);

        //Verifica se registro foi encontrado
        if ($livro) {
            $livro->update($dados);

            $retorno = array(
                "mensagem" => 'Livro atualizado com sucesso',
                "dados" => $livro
            );
    
            return response()->json($retorno);
        }
        else {
            $retorno = array(
                "mensagem" => 'ID não encontrado'
            );
            
            return response()->json($retorno, 404);
        }
    }

    public function delete($id) {
        $livro = Livro::find($id);
        if ($livro) {
            $livro->delete($id);

            return response()->json($retorno);
        }
        else {
            $retorno = array(
                "mensagem" => 'ID não encontrado'
            );
            
            return response()->json($retorno, 404);
        }
        
    }
}
